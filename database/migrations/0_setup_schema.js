const Schema = use('Schema')

class SetupSchema extends Schema {
  async up() {
    await this.db.raw('CREATE EXTENSION IF NOT EXISTS "pgcrypto" schema public')

    await this.db.raw('CREATE EXTENSION IF NOT EXISTS "postgis" schema public')
  }

  async down() {
    await this.db.raw('DROP EXTENSION IF EXISTS "pgcrypto"')
    await this.db.raw('DROP EXTENSION IF EXISTS "postgis"')
  }
}

module.exports = SetupSchema
