const Schema = use('Schema');

class RolePermissionSchema extends Schema {
  up() {
    this.create('roles_permissions', (table) => {
      table
        .uuid('id')
        .unique()
        .defaultTo(this.db.raw('public.gen_random_uuid()'))
        .primary();
      table
        .uuid('permission_id')
        .references('id')
        .inTable('permissions')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');
      table
        .uuid('role_id')
        .references('id')
        .inTable('roles')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');
      table.timestamps();
    });
  }

  down() {
    this.drop('roles_permissions');
  }
}

module.exports = RolePermissionSchema;
