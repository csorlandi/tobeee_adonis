/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class LoginlogSchema extends Schema {
  up() {
    this.create('login_logs', (table) => {
      table
        .uuid('id')
        .unique()
        .defaultTo(this.db.raw('public.gen_random_uuid()'))
        .primary();
      table
        .uuid('user_id')
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');
      table.string('ip');
      table.string('where');
      table.string('device');
      table.timestamps();
    });
  }

  down() {
    this.drop('login_logs');
  }
}

module.exports = LoginlogSchema;
