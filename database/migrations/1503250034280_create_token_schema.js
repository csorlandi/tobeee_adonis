const Schema = use('Schema');

class TokensSchema extends Schema {
  up() {
    this.create('tokens', (table) => {
      table
        .uuid('id')
        .unique()
        .defaultTo(this.db.raw('public.gen_random_uuid()'))
        .primary();
      table
        .uuid('user_id')
        .references('id')
        .inTable('users');
      table
        .text('token')
        .notNullable()
        .unique()
        .index();
      table.string('type').notNullable();
      table.boolean('is_revoked').defaultTo(false);
      table.timestamps();
    });
  }

  down() {
    this.drop('tokens');
  }
}

module.exports = TokensSchema;
