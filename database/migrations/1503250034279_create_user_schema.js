const Schema = use('Schema');

class UserSchema extends Schema {
  up() {
    this.create('users', (table) => {
      table
        .uuid('id')
        .unique()
        .defaultTo(this.db.raw('public.gen_random_uuid()'))
        .primary();
      table.string('name', 254).notNullable();
      table
        .string('email')
        .unique()
        .notNullable();
      table.string('password');
      table.string('avatar');
      table.text('token');
      table.datetime('token_created_at');
      table.text('google_id');
      table.text('google_token');
      table.text('facebook_id');
      table.text('facebook_token');
      table.string('study_choice');
      table.timestamps();
    });
  }

  down() {
    this.drop('users');
  }
}

module.exports = UserSchema;
