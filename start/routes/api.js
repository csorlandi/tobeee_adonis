const Route = use('Route');

Route.post(
  'authenticated/facebook',
  'FacebookSessionController.callback',
).middleware('request-info-parser');

Route.post(
  'authenticated/google',
  'GoogleSessionController.callback',
).middleware('request-info-parser');

Route.post('users', 'UserController.store');
Route.put('users', 'UserController.update');

Route.post('sessions', 'SessionController.store').middleware(
  'request-info-parser',
);
