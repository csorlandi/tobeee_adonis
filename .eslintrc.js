module.exports = {
  env: {
    es6: true,
    jest: true
  },
  globals: {
    use: true
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module'
  },
  plugins: ['import', 'import-helpers', 'prettier'],
  extends: ['airbnb-base', 'prettier'],
  rules: {
    'prettier/prettier': 'error',
    'no-empty': [
      'warn',
      {
        allowEmptyCatch: true
      }
    ],
    'no-underscore-dangle': 'off',
    'no-console': ['error', { allow: ['error'] }],
    'import/named': 'off',
    'import/export': 'off',
    'import/prefer-default-export': 'off',
    'import/no-unresolved': 'error',
    'class-methods-use-this': 'off',
    'no-param-reassign': 'off',
    'no-unused-expressions': [
      'warn',
      {
        allowShortCircuit: true,
        allowTernary: true
      }
    ]
  }
}
