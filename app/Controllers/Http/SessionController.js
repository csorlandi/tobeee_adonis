const GeneralException = use('App/Exceptions/GeneralException');

const User = use('App/Models/User');

const LoginLogger = use('App/Services/LoginLogger');

class SessionController {
  async store({ request, auth }) {
    const { email, password } = request.only(['email', 'password']);

    const user = await User.findBy('email', email);

    if (!user) {
      throw new GeneralException('session.invalid_credentials', 401);
    }

    if (
      request.url().includes('admin') &&
      !((await user.can('login_admin')) || (await user.is('admin')))
    ) {
      throw new GeneralException('session.not_allowed', 403);
    }

    if (user.facebook_id && !user.google_id)
      throw new GeneralException('user.facebook_exists_before_email');

    if (!user.facebook_id && user.google_id)
      throw new GeneralException('user.google_exists_before_email');

    if (user.facebook_id && user.google_id)
      throw new GeneralException('user.all_social_exists_before_email');

    const { id, name, avatar } = user.toJSON();

    try {
      const payload = {
        id,
        name,
        email,
        avatar,
      };

      const token = await auth.attempt(email, password, payload);

      await LoginLogger.handle(request, user);

      return {
        user: {
          id: user.id,
          name: user.name,
          email: user.email,
          study_choice: user.study_choice,
        },
        token,
      };
    } catch (err) {
      throw new GeneralException('session.invalid_credentials', 401);
    }
  }
}

module.exports = SessionController;
