const User = use('App/Models/User');
const GeneralException = use('App/Exceptions/GeneralException');
const LoginLogger = use('App/Services/LoginLogger');

class GoogleSessionController {
  async redirect({ ally }) {
    await ally.driver('google').redirect();
  }

  async callback({ auth, ally, request }) {
    try {
      const { access_token } = request.all();

      const googleUser = await ally
        .driver('google')
        .getUserByToken(access_token);

      const { name, email, sub: id } = googleUser.getOriginal();

      let user = await User.query()
        .where('google_id', id)
        .orWhere('email', email)
        .first();

      if (user) {
        if (user.facebook_id && !user.google_id) {
          return {
            facebook_account_exists: true,
            user: {
              id: user.id,
              name,
              email,
              google_id: id,
              google_token: googleUser.getAccessToken(),
            },
          };
        }
      } else {
        user = await User.create({
          name,
          email,
          google_id: id,
          google_token: googleUser.getAccessToken(),
          avatar: googleUser.getAvatar(),
        });
      }

      const payload = {
        id,
        avatar: googleUser.getAvatar(),
        name,
        email,
      };

      const token = await auth.generate(user, payload);

      await LoginLogger.handle(request, user);

      return {
        user: { id: user.id, name, email, study_choice: user.study_choice },
        token,
      };
    } catch (err) {
      console.log(err);
      throw new GeneralException('session.google_login_failed');
    }
  }
}

module.exports = GoogleSessionController;
