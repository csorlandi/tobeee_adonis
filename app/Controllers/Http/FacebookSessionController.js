const User = use('App/Models/User');
const GeneralException = use('App/Exceptions/GeneralException');
const LoginLogger = use('App/Services/LoginLogger');

class FacebookSessionController {
  async redirect({ ally }) {
    await ally.driver('facebook').redirect();
  }

  async callback({ auth, ally, request }) {
    try {
      const { access_token } = request.all();

      const facebookUser = await ally
        .driver('facebook')
        .getUserByToken(access_token);

      const { id, name, email } = facebookUser.getOriginal();

      let user = await User.query()
        .where('facebook_id', id)
        .orWhere('email', email)
        .first();

      if (user) {
        if (user.google_id && !user.facebook_id) {
          return {
            google_account_exists: true,
            user: {
              id: user.id,
              name,
              email,
              facebook_id: id,
              facebook_token: facebookUser.getAccessToken(),
            },
          };
        }
      } else {
        user = await User.create({
          name,
          email,
          facebook_id: id,
          facebook_token: facebookUser.getAccessToken(),
          avatar: facebookUser.getAvatar(),
        });
      }

      const payload = {
        id,
        avatar: facebookUser.getAvatar(),
        name,
        email,
      };

      const token = await auth.generate(user, payload);

      await LoginLogger.handle(request, user);

      return {
        user: { id: user.id, name, email, study_choice: user.study_choice },
        token,
      };
    } catch (err) {
      console.log(err);
      throw new GeneralException('session.facebook_login_failed');
    }
  }
}

module.exports = FacebookSessionController;
