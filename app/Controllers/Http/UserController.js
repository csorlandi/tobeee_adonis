const User = use('App/Models/User');
const GeneralException = use('App/Exceptions/GeneralException');

class UserController {
  async store({ request }) {
    const data = request.only(['name', 'email', 'password', 'study_choice']);

    let user = await User.findBy('email', data.email);

    if (user) {
      if (user.facebook_id && !user.google_id)
        throw new GeneralException('user.facebook_exists_before_email');

      if (!user.facebook_id && user.google_id)
        throw new GeneralException('user.google_exists_before_email');

      if (user.facebook_id && user.google_id)
        throw new GeneralException('user.all_social_exists_before_email');

      throw new GeneralException('user.already_registered');
    }

    user = await User.create(data);

    const { id, name, email, study_choice } = user;

    return { user: { id, name, email, study_choice } };
  }

  async update({ request }) {
    const { id, google_id, google_token } = request.all();

    const user = await User.findBy('id', id);

    if (user) {
      user.merge({
        google_id,
        google_token,
      });

      await user.save();

      const { name, email } = user;

      return { user: { name, email } };
    }

    throw new GeneralException('user.not_found');
  }
}

module.exports = UserController;
