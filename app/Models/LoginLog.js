/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const format = require('date-fns/format');
const pt = require('date-fns/locale/pt');

const Model = use('Model');

class LoginLog extends Model {
  static boot() {
    super.boot();
  }

  static get computed() {
    return ['time'];
  }

  getTime({ created_at: createdAt }) {
    return format(createdAt, 'dddd, DD [de] MMMM [de] YYYY [às] HH:mm', {
      locale: pt,
    });
  }
}

module.exports = LoginLog;
