const Hash = use('Hash');

const UserHook = {};
exports = UserHook;
module.exports = UserHook;

UserHook.hashPassword = async (userInstance) => {
  if (userInstance.dirty.password) {
    userInstance.password = await Hash.make(userInstance.password);
  }
};
