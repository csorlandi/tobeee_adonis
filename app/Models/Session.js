/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class Session extends Model {
  static boot() {
    super.boot();

    this.addTrait('@provider:Timezone/Trait');
  }

  user() {
    return this.belongsTo('App/Models/User');
  }
}

module.exports = Session;
