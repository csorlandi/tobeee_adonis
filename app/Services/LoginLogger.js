const LoginLog = use('App/Models/LoginLog');
const GeneralException = use('App/Exceptions/GeneralException');

class LoginLogger {
  async handle(request, user) {
    const { ip, useragent, geoip } = request;

    let device = '';
    if (useragent) {
      device = `${useragent.browser.family} | ${useragent.device.family} | ${useragent.os.family}`;
    }

    let where = '';
    if (geoip) {
      where = `${geoip.city} | ${geoip.region} | ${geoip.country}`;
    }

    if (!user) throw new GeneralException('logger.without_user');

    await LoginLog.create({
      user_id: user.id,
      ip,
      where,
      device,
    });
  }
}

module.exports = new LoginLogger();
