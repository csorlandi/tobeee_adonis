const Env = use('Env');

const useragent = use('useragent');
const geoip = use('geoip-lite');

class RequestInfoParser {
  async handle({ request }, next) {
    const headers = request.headers();
    const agent = useragent.parse(headers['user-agent']);

    request.useragent = {
      browser: {
        family: agent.family,
        version: agent.major,
      },
      device: {
        family: agent.device.family,
        version: agent.device.major,
      },
      os: {
        family: agent.os.family,
        major: agent.os.major,
        minor: agent.os.minor,
      },
    };

    request.acceptHeaders = {
      accept: headers.accept || '',
      language: headers['accept-language'] || '',
    };

    /* istanbul ignore next */
    const ip =
      Env.get('NODE_ENV') === 'development' ? '127.0.0.1' : request.ip();
    const geo = geoip.lookup(ip);

    request.ip = ip;
    request.geoip = geo || null;

    await next();
  }
}

module.exports = RequestInfoParser;
