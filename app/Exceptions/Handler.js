const BaseExceptionHandler = use('BaseExceptionHandler');
const Env = use('Env');
const Youch = use('Youch');

const Sentry = use('Sentry');

const reportNotNeeded = [
  'GeneralException',
  'ValidationException',
  'ForbiddenException',
  'HttpException',
  'ExpiredJwtToken',
  'SyntaxError',
];

class ExceptionHandler extends BaseExceptionHandler {
  /**
   * Handle exception thrown during the HTTP lifecycle
   *
   * @method handle
   *
   * @param  {Object} error
   * @param  {Object} options.request
   * @param  {Object} options.response
   *
   * @return {void}
   */
  async handle(error, { response, request }) {
    const errorStatus = error.status || 500;

    if (error.code === 'E_MISSING_DATABASE_ROW') {
      return response.status(errorStatus).send({
        error: {
          code: error.code,
          message: error.message.replace(/(.*: )(.*)(\n.*){0,}/, '$2'),
        },
      });
    }

    if (error.name === 'GeneralException') {
      return response
        .status(errorStatus)
        .send({ error: { message: error.message } });
    }

    if (error.name === 'ValidationException') {
      return response.status(errorStatus).send(error.messages);
    }

    if (Env.get('NODE_ENV') === 'development') {
      console.error(error);
      const youch = new Youch(error, request.request);
      const errorJSON = await youch.toJSON();

      return response.status(error.status).send(errorJSON);
    }

    return response.status(errorStatus).send(error.message);
  }

  /**
   * Report exception for logging or debugging.
   *
   * @method report
   *
   * @param  {Object} error
   * @param  {Object} options.request
   * @param  {Object} options.auth
   *
   * @return {void}
   */
  async report(error, { request, auth: { user } }) {
    if (reportNotNeeded.includes(error.name)) {
      return;
    }

    Sentry.configureScope((scope) => {
      scope.setExtra('request', {
        url: request.originalUrl(),
        method: request.method(),
        headers: request.headers(),
        body: request.all(),
      });
      if (user) {
        scope.setUser(user.toObject());
      }
    });

    Sentry.captureException(error);
  }
}

module.exports = ExceptionHandler;
