const Antl = use('Antl');

const { LogicalException } = require('@adonisjs/generic-exceptions');

class GeneralException extends LogicalException {
  constructor(key, status = 400, bypassLocalization = false) {
    const message = bypassLocalization ? key : Antl.formatMessage(key);
    super(message, status, '');
  }
}

module.exports = GeneralException;
